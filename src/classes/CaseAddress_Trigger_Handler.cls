/*
    Handler class for Trigger_CaseAddress trigger
	
    ======================================================
    Changes:
    	2017 July, Davanti - YK Created
*/

public with sharing class CaseAddress_Trigger_Handler {
	
	public void OnBeforeUpdate(List<CaseAddress__c> listCaseAddresses, Map<Id, CaseAddress__c> mapOldMap){
        for (CaseAddress__c ca : listCaseAddresses) {
            ca.Bypassed_by_API__c = false;
        }
		checkAddressChange(listCaseAddresses, mapOldMap);
	}
	
	public void OnAfterInsert(List<CaseAddress__c> listCaseAddresses, Map<Id, CaseAddress__c> mapNewMap){
		//setValidatedAddressDPID(mapNewMap.keySet());
		checkAddressChange(listCaseAddresses);
	}
	
	
	// SERVICE NZPCRM-832 & NZPCRM-529 YK START
	public void checkAddressChange(List<CaseAddress__c> listCaseAddresses){
		
		Set<Id> setCaseAddressIdsToProcess = new Set<Id>();
		Set<Id> setCaseAddressIdsToReset = new Set<Id>();
		List<CaseAddress__c> listCaseAddressesToUpdate = new List<CaseAddress__c>();
		
		for(CaseAddress__c ca : listCaseAddresses){
			if(ca.Address_DPID_Updated__c){
				//ca.Address_DPID_Updated__c = false;
				setCaseAddressIdsToReset.add(ca.Id);
			}
			else{
                if(!ca.Bypassed_by_API__c) {
				setCaseAddressIdsToProcess.add(ca.Id);
                    }
			}
		}
		
		if(setCaseAddressIdsToProcess.size() > 0){
			setValidatedAddressDPID(setCaseAddressIdsToProcess);
		}
		
		if(setCaseAddressIdsToReset.size() > 0){
			for(CaseAddress__c ca : [SELECT Id, Address_DPID_Updated__c FROM CaseAddress__c WHERE Id IN :setCaseAddressIdsToReset]){
				ca.Address_DPID_Updated__c = false;
				listCaseAddressesToUpdate.add(ca);
			}
			if(listCaseAddressesToUpdate.size() > 0)
				update listCaseAddressesToUpdate;
		}
	}
	// SERVICE NZPCRM-832 & NZPCRM-529 YK END
	
	
	// SERVICE NZPCRM-832 & NZPCRM-529 YK START
	public void checkAddressChange(List<CaseAddress__c> listCaseAddresses, Map<Id, CaseAddress__c> mapOldMap){
		
		Set<Id> setCaseAddressIds = new Set<Id>();
		
		for(CaseAddress__c ca : listCaseAddresses){
			if(ca.Address_DPID_Updated__c){
				ca.Address_DPID_Updated__c = false;
			}
			else{
				if(ca.Street__c != mapOldMap.get(ca.Id).Street__c
					|| ca.Suburb__c != mapOldMap.get(ca.Id).Suburb__c
					|| ca.City__c != mapOldMap.get(ca.Id).City__c
					|| ca.Postcode__c != mapOldMap.get(ca.Id).Postcode__c
					|| ca.Country__c != mapOldMap.get(ca.Id).Country__c
				){
					setCaseAddressIds.add(ca.Id);
				}
			}
		}
		
		if(setCaseAddressIds.size() > 0){
			setValidatedAddressDPID(setCaseAddressIds);
		}
	}
	// SERVICE NZPCRM-832 & NZPCRM-529 YK END
	
	
	// SERVICE NZPCRM-832 & NZPCRM-529 YK START
	@future (callout=true)
	public static void setValidatedAddressDPID(Set<Id> setCaseAddressIds){
		
		String strSearch;
		Set<String> setNZ = new Set<String>{'new zealand', 'nz', 'nzl'};
		List<CaseAddress__c> listCaseAddresses = [SELECT Street__c, Suburb__c, City__c, Postcode__c, Country__c, DPID__c FROM CaseAddress__c WHERE Id IN :setCaseAddressIds]; 
		
		for(CaseAddress__c ca : listCaseAddresses){
			strSearch = '';
			if(String.isNotBlank(ca.Street__c)
				&& (String.isNotBlank(ca.Suburb__c) || String.isNotBlank(ca.City__c))
				&& String.isNotBlank(ca.Postcode__c) 
				&& String.isNotBlank(ca.Country__c) 
				&& setNZ.contains(ca.Country__c.toLowerCase())
			){
				strSearch = ca.Street__c + ' ' 
							+ (String.isNotBlank(ca.Suburb__c) ? ca.Suburb__c + ' ' : '')
							+ (String.isNotBlank(ca.City__c) ? ca.City__c + ' ' : '') 
							+ ca.Postcode__c;
				
				String strSearchSafe = String.escapeSingleQuotes(strSearch);
				List<AddressController.ValidatedAddress> listResultAddresses = AddressController.getAddresses(strSearchSafe);
				if(listResultAddresses.size() > 0){
					ca.DPID__c = listResultAddresses[0].DPID;
				}
				else{
					ca.DPID__c = null;
				}
			}
			else{
				ca.DPID__c = null;
			}
		}
		
		if(listCaseAddresses.size() > 0)
			update listCaseAddresses;
	}
	// SERVICE NZPCRM-832 & NZPCRM-529 YK END
	
}