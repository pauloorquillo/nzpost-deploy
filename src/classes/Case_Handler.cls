/*************************************************************************************************************************************************
Class Name  : CaseEmailMessageEmailService.cls
Description :

Change Log
----------
Author              Created Date                Description
Geoffry                                         Created
Venoda              10/10/2017                  Wriiten code for handing record locking issue
Ryan Winckel        27/11/2017                  caseAssignmentRule if check to stop null error
**************************************************************************************************************************************************/
public class Case_Handler{

    public static string STATUS_SUCCESS    = 'Success';
    public static string STATUS_FAILED     = 'Failed';
    public static string STATUS_QUEUED     = 'Queued';
    public static string STATUS_PROCESSING = 'Processing';
    public static Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NCC Enquire').getRecordTypeId();
    public static String emailToCaseAddress= 'BLT.SupplyOrders@nzpost.co.nz';
    public static AssignmentRule caseAssignmentRule = null;

    public Case_Handler(){
        if (userinfo.getUserType() == 'Standard'){
            caseAssignmentRule = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        }  
    }

    public Case createCase(String origin,String description,String subject,String suppliedEmail,String suppliedName,String emailToCaseAddress, Id contactId){
        system.debug('createCase - &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& - Starts');
        Case caseRecord = new Case();
        caseRecord.Origin = origin;
        caseRecord.Description = description;
        caseRecord.Subject = subject;
        caseRecord.SuppliedEmail = suppliedEmail;
        caseRecord.SuppliedName = suppliedName;
        caseRecord.RecordTypeId = caseRecordTypeId;
        caseRecord.Email_to_Case_Address__c = emailToCaseAddress;
        caseRecord.ContactId = contactId;
        system.debug('createCase - &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& - Ends');
        return caseRecord;
    }

    public EmailMessage createEmailMessage(String fromAddress,String fromName,String toAddress,String subject,String plainTextBody,String htmlBody,Id caseId,String status,Boolean inComing){
        system.debug('createEmailMessage - &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& - Starts');
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = fromAddress;
        newEmail.FromName = fromName;
        newEmail.ToAddress = toAddress;
        newEmail.Subject = subject;
        newEmail.TextBody = plainTextBody;
        newEmail.HtmlBody = htmlBody;
        newEmail.ParentId = caseId;
        newEmail.Status = status;
        newEmail.incoming = inComing;
        system.debug('createEmailMessage - &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& - Ends');
        return newEmail;
    }

    public Case_Email_Message__c createEmailMessage(Id caseId,String threadId,Id existingCaseId,string fromAddress,string fromName,string htmlBody,string plainTextBody,string subject,string toAddresses, Id contactId){
        Case_Email_Message__c cem = new Case_Email_Message__c();
        cem.Case__c = caseId;
        cem.Thread_Id__c = threadId;
        cem.Case_Id_From_Thread_Id__c = existingCaseId;
        cem.FromAddress__c = fromAddress;
        cem.FromName__c = fromName;
        cem.HTML_Body__c = htmlBody;
        cem.Plain_Text_Body__c = plainTextBody;
        cem.Subject__c = subject;
        cem.ToAddress__c = toAddresses;
        cem.Status__c = STATUS_QUEUED;
        cem.ContactId__c = contactId;
        return cem;
    }

    public void reassignCaseWithActiveRule(Case caseRecord) {
        //recreating the DMLOptions setting for "Assign using active assignment rules" checkbox on Case object...
        if (caseAssignmentRule != null) {
        	Database.DMLOptions dmlOpts = new Database.DMLOptions();      
        	dmlOpts.assignmentRuleHeader.assignmentRuleId= caseAssignmentRule.id;
        	caseRecord.setOptions(dmlOpts);
        }
    }

    public string preparePlainBody(String caseNumber,String caseThreadId){
        return 'Dear Purple House \n\n Case Number: '+caseNumber+'\n\n Thank you for contacting NZ Post.\n\n A Customer Services Representative will be in touch again soon.\n\n Kind regards\n\n National Contact Centre \n\n'+caseThreadId;
    }

    public string prepareHTMLBody(String caseNumber,String caseThreadId){
        return '<html><body><font size="2" face="arial,sans-serif"><br><br> Dear Purple House <br><br> Case Number: '+caseNumber+'<br><br> Thank you for contacting NZ Post.<br><br> A Customer Services Representative will be in touch again soon.<br><br> Kind regards<br><br> National Contact Centre <br></font><p style="text-align:right;font-size:10px;">'+caseThreadId+'</p></body></html>';
    }

    public void addAttachmentsfrmEmail(Messaging.inboundEmail email, Id caseId){
        // Add any binary attachments to the case
        if (email.binaryAttachments!=null && email.binaryAttachments.size() > 0) {
            for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                try {
                    if (email.binaryAttachments[i].filename != null) {
                        Attachment newAttachment = new Attachment(ParentId = caseId,
                                                                  Name = email.binaryAttachments[i].filename,
                                                                  Body = email.binaryAttachments[i].body);
                        insert newAttachment;
                        System.debug('New Binary Attachment: ' + newAttachment );
                    }
                }
                catch (Exception e) {
                    System.debug('\n\nError:: ' + e + '\n\n');
                }
            }
        }

        // Add any text attachments to the case
        if (email.textAttachments!=null && email.textAttachments.size() > 0) {
            for (integer i = 0 ; i < email.textAttachments.size() ; i++) {
                try {
                    if (email.textAttachments[i].filename != null) {
                        Attachment newAttachment = new Attachment(ParentId = caseId,
                                                                  Name = email.textAttachments[i].filename,
                                                                  Body = Blob.valueOf(email.textAttachments[i].body) );
                        insert newAttachment;
                        System.debug('New Text Attachment: ' + newAttachment );
                    }
                }
                catch (Exception e) {
                    System.debug('\n\nError:: ' + e + '\n\n');
                }
            }
        }
    }

    public String getIdFromThreadId(String threadId){ //assumes this format _00D0lCsKM._5000l1Joyo

        //This: 5000l1Joyo
        //Returns: 5000l000001Joyo

        String caseId = '';
        String tempString = '';

        if(threadId.contains('._')){

            tempString = threadId.substringAfter('._');

            caseid = tempString.left(5) + '00000' + tempString.right(5);

            return caseid;

        }else{
            return '';
        }

    }
}