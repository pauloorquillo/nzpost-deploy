@isTest global class Test_ParcelDetailsGetCalloutMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HTTPResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"success": true,"results": {"message_id": "74846c10-2c88-11e8-98ab-0611b993587c","tracking_reference": "8441113030081401WLG006ET","message_datetime": "2018-03-19T19:48:41Z","service": "","carrier":"","tracking_events": [{"tracking_reference": "8441113030081401WLG006ET","source": "CME","seqref": "3436470376","status_description": "Online Manifest","event_description": "A trackinglabel has been created for your item, but your item has not yet been picked up. Turn on delivery updates to receive notifications on your item progress","event_datetime":"2018-01-19T10:11:23","signed_by": {"name": "","signature": ""},"event_edifact_code": "997"},{"tracking_reference": "8441113030081401WLG006ET","source": "CME","seqref":"3436470527","status_description": "Job Booking","event_description": "We have received your request to have this parcel redelivered","event_datetime": "2018-01-19T11:02:16","signed_by": {"name": "","signature": ""},"event_edifact_code": "201"}]}}' ); 
        response.setStatusCode(200);
        return response;
    }
}