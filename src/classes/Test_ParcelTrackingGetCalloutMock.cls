@isTest global class Test_ParcelTrackingGetCalloutMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HTTPResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"success": true, "message_id": "5d4a848c-b6b6-4ad2-a2d7-144a9ab34380", "authentication_email": null, "parcels":[{"tracking_reference": "8441113030081401WLG006ET", "base_tracking_reference": null, "carrier": "CourierPost", "merchant": {"id": "91327067", "name": "ECL IT Online (PRD) 12.05.10"},"receiver_details": {"name": "REDELIVERY","phone": "0","address": {"suburb": "REDELIVERY","country": "New Zealand"}},"product": {"service_code": "CPOLCT2","description": "CP Online I/Island Contract","service_standard": "OVERNIGHT","is_signature_required":true,"is_rural": false,"is_saturday": false,"is_evening": false,"is_no_atl": false,"is_dangerous_goods": false},"weight_kg": 6}]}');
        response.setStatusCode(200);
        return response;
    }
}