@isTest global class Test_ParcelTrackingPostCalloutMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HTTPResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"access_token":"testToken","token_type": "Bearer","expires_in": 86399}');
        response.setStatusCode(200);
        return response;
    }
}