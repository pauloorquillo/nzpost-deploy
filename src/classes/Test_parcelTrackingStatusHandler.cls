@isTest
private class Test_parcelTrackingStatusHandler {
    
    @isTest static void testParcelTrackingInit(){
        Case c = new Case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NCC Enquire').getRecordTypeId();
        c.Subject = 'Test';
        c.Failure_Type__c ='Delayed delivery';
        c.Description = 'Test';
        c.NCC_Track_Trace_Number__c = '12354';
        
        testPostCallout();
        
        Test.setMock(HttpCalloutMock.class, new Test_ParcelDetailsGetCalloutMock());
        HttpResponse response = parcelTrackingStatusHandler.makeGetCallout('testToken','12354','testId','testSecret');
        String contentType = response.getHeader('Content-Type');
        
        NZ_Post_API_Credentials__c apiSettings1= new NZ_Post_API_Credentials__c();
        apiSettings1.Name = 'NZ Post Oauth API';
        apiSettings1.API_URL__c = 'http://www.google.co.nz';
        apiSettings1.client_id__c = 'testId';
        apiSettings1.client_secret__c ='testSecret';
        insert apiSettings1;
        
        NZ_Post_API_Credentials__c apiSettings2= new NZ_Post_API_Credentials__c();
        apiSettings2.Name = 'NZ Post ParcelTracking API';
        apiSettings2.API_URL__c = 'http://www.google.co.nz';
        apiSettings2.client_id__c = 'testId';
        apiSettings2.client_secret__c ='testSecret';
        insert apiSettings2;
        
        NZ_Post_API_Credentials__c apiSettings3= new NZ_Post_API_Credentials__c();
        apiSettings3.Name = 'NZ Post ParcelDetails API';
        apiSettings3.API_URL__c = 'http://www.google.co.nz';
        apiSettings3.client_id__c = 'testId';
        apiSettings3.client_secret__c ='testSecret';
        insert apiSettings3;
      
        test.startTest();
        insert c;
        c.Fetch_parcel_details__c = false;
        c.Fetch_Delivery_Status__c = true;
        update c;
        test.stopTest();
        
 
    }
    
    @isTest static void testParcelTrackingInit2(){
        Case c = new Case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('NCC Enquire').getRecordTypeId();
        c.Subject = 'Test';
        c.Failure_Type__c ='Delayed delivery';
        c.Description = 'Test';
        c.NCC_Track_Trace_Number__c = '12354';
        
        testPostCallout();
        
        Test.setMock(HttpCalloutMock.class, new Test_ParcelTrackingGetCalloutMock());
        HttpResponse response = parcelTrackingStatusHandler.makeGetCallout('testToken','12354','testId','testSecret');
        String contentType = response.getHeader('Content-Type');
        
        NZ_Post_API_Credentials__c apiSettings1= new NZ_Post_API_Credentials__c();
        apiSettings1.Name = 'NZ Post Oauth API';
        apiSettings1.API_URL__c = 'http://www.google.co.nz';
        apiSettings1.client_id__c = 'testId';
        apiSettings1.client_secret__c ='testSecret';
        insert apiSettings1;
        
        NZ_Post_API_Credentials__c apiSettings2= new NZ_Post_API_Credentials__c();
        apiSettings2.Name = 'NZ Post ParcelTracking API';
        apiSettings2.API_URL__c = 'http://www.google.co.nz';
        apiSettings2.client_id__c = 'testId';
        apiSettings2.client_secret__c ='testSecret';
        insert apiSettings2;
        
        NZ_Post_API_Credentials__c apiSettings3= new NZ_Post_API_Credentials__c();
        apiSettings3.Name = 'NZ Post ParcelDetails API';
        apiSettings3.API_URL__c = 'http://www.google.co.nz';
        apiSettings3.client_id__c = 'testId';
        apiSettings3.client_secret__c ='testSecret';
        insert apiSettings3;
      
        test.startTest();
        insert c;
        c.Fetch_parcel_details__c = true;
        c.Fetch_Delivery_Status__c = false;
        update c;
        test.stopTest();
    }
    

    
    @isTest static void testGetToken() {
        testPostCallout();
        
        NZ_Post_API_Credentials__c apiSettings = new NZ_Post_API_Credentials__c();
        apiSettings.Name = 'NZ Post Oauth API';
        apiSettings.API_URL__c = 'http://www.google.co.nz';
        apiSettings.client_id__c = 'testId';
        apiSettings.client_secret__c ='testSecret';
        insert apiSettings;
        
        Test.startTest();
        parcelTrackingStatusHandler.getToken();
        Test.stopTest();
    }
    

    
    
    @isTest static void testPostCallout() {
        Test.setMock(HttpCalloutMock.class, new Test_ParcelTrackingPostCalloutMock()); 
        HttpResponse response = parcelTrackingStatusHandler.makePostCallout('Http://www.google.co.nz');
        // Verify that the response received contains fake values
        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = response.getBody();
        String expectedValue = '{"access_token":"testToken","token_type": "Bearer","expires_in": 86399}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, response.getStatusCode());
    }
    
        @isTest static void testMakeGetCallout() {
        testGetCallout();
        
        NZ_Post_API_Credentials__c apiSettings = new NZ_Post_API_Credentials__c();
        apiSettings.Name = 'NZ Post ParcelTracking API';
        apiSettings.API_URL__c = 'http://www.google.co.nz';
        apiSettings.client_id__c = 'testId';
        apiSettings.client_secret__c ='testSecret';
        insert apiSettings;
        
        Test.startTest();
        parcelTrackingStatusHandler.makeGetCallout('test', 'test', 'test', 'test');
        Test.stopTest();
    }
    
    @isTest static void testGetCallout() {
        Test.setMock(HttpCalloutMock.class, new Test_ParcelTrackingGetCalloutMock());
        HttpResponse response = parcelTrackingStatusHandler.makeGetCallout('test', 'test', 'testId', 'testSecret');
        String contentType = response.getHeader('Content-Type');
    }
    
        @isTest static void testGetCallout2() {
        Test.setMock(HttpCalloutMock.class, new Test_ParcelDetailsGetCalloutMock());
        HttpResponse response = parcelTrackingStatusHandler.makeGetCallout('test', 'test', 'testId', 'testSecret');
        String contentType = response.getHeader('Content-Type');
    }
}