public class parcelTrackingStatusHandler {
    
    //Called from case_trigger on DML after insert/update
    //Checks for flags set via UI and will init below method if either has been set
    //Passes Ids because cannot call future methods directly from trigger
    public static void parcelTrackingInit(List<Case> caseTriggerNew){
        List<Id> caseIds = new List<Id>();
        
        for (case c : caseTriggerNew) {
            if (c.Fetch_Delivery_Status__c || c.Fetch_parcel_details__c) {
                caseIds.add(c.Id);
            }
            if (caseIds.size()>0) {
                getDetailsAndTracking(caseIds);
            }
        }
    }    
    @future(callout=true)
    public static void getDetailsAndTracking(List<Id> caseIds) {
        List<Case>listCasesToUpdate = new List<Case>();
        List<CaseAddress__c> listCaseContactsToInsert = new List<CaseAddress__c>();
        
        String token = getToken();
        NZ_Post_API_Credentials__c parcelTrackingAPI = NZ_Post_API_Credentials__c.getInstance('NZ Post ParcelTracking API');
        NZ_Post_API_Credentials__c parcelDetailsAPI = NZ_Post_API_Credentials__c.getInstance('NZ Post ParcelDetails API');
        
        List<Case> listCaseQuery = [SELECT Id, Fetch_Delivery_Status__c, Fetch_parcel_details__c, NCC_Track_Trace_Number__c FROM Case WHERE Id IN:caseIds];
        for (case c : listCaseQuery) {
            c.API_Response__c = '';
            if (c.NCC_Track_Trace_Number__c != null) {
                if (c.Fetch_Delivery_Status__c==true) {
                    HttpResponse responseTracking = makeGetCallout(token, c.NCC_Track_Trace_Number__c, parcelTrackingAPI.API_URL__c, parcelTrackingAPI.client_id__c);
                    if(responseTracking!= null) {
                        Map<String, Object> responseBody = (Map<String, Object>)JSON.deserializeUntyped(responseTracking.getBody());
                        Map<String,Object> results = (Map<String,Object>) responseBody.get('results');
                        List<Object> trackingEventsList = (List<Object>) results.get('tracking_events');
                        if (trackingEventsList != null) {
                            Map<String,Object> trackingEvent = (Map<String,Object>) trackingEventsList.get(trackingEventsList.size()-1);
                            String eventDescription = (String) trackingEvent.get('event_description');
                            String eventDateTime = (String) trackingEvent.get('event_datetime');
                            String statusDescription = (String) trackingEvent.get('status_description');
                            String signedBy = (String) trackingEvent.get('signed_by.name');
                            
                            
                            c.Event_Description__c = eventDescription;	
                            c.Event_Date_Time__c = eventDateTime;
                            c.Status_Description__c = statusDescription;
                            c.Signed_By__c = signedBy;
                            
                        } else {
                            List<Object> errors = (List<Object>) responseBody.get('errors');
                            Map<String,Object> error = (Map<String,Object>) errors.get(0);
                            c.API_Response__c += 'ParcelTracking: ' + (String) error.get('details') + ' \n';
                        }
                    }
                }
                if (c.Fetch_parcel_details__c==true) {
                    HttpResponse response = makeGetCallout(token, c.NCC_Track_Trace_Number__c, parcelDetailsAPI.API_URL__c, parcelDetailsAPI.client_id__c);
                    
                    Map<String, Object> responseBody = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                    if (responsebody != null) {
                        if(response.getStatusCode() !=200) {
                            c.API_Response__c += 'ParcelDetails: ' + response.getStatus();
                        }
                        List<Object> parcels = (List<Object>) responseBody.get('parcels');
                        if (parcels!= null){
                            Map<String,Object> parcel = (Map<String,Object>) parcels.get(0);
                            Map<String,Object> product = (Map<String,Object>) parcel.get('product');
                            
                            if (product!=null) {
                                c.NCC_Weight_of_Item__c = (Decimal) parcel.get('weight_kg');
                                c.Contents_Description__c = (String) product.get('description');
                                Boolean dangerousGoodsFlag = (boolean) product.get('is_dangerous_goods');
                                if (dangerousGoodsFlag==true) {
                                    c.NCC_Description_Dangerous_Prohibited__c = 'This item has been flagged as Dangerous Goods.';
                                }
                                Map<String,Object> sender_details = (Map<String,Object>) parcel.get('sender_details');
                                
                                if (sender_details != null) {
                                    listCaseContactsToInsert.add(createCaseContact(c.Id, sender_details, 'Sender', true));
                                }
                                Map<String,Object> receiver_details = (Map<String,Object>) parcel.get('receiver_details');
                                
                                if (receiver_details != null) {
                                    listCaseContactsToInsert.add(createCaseContact(c.Id, receiver_details, 'Addressee', false));
                                }
                                Map<String,Object> value = (Map<String,Object>) parcel.get('value');
                                if (value!=null){
                                    c.NCC_Contents_Price__c = (Decimal) value.get('amount');
                                }   
                            } else {
                                Map<String,Object> errors = (Map<String,Object>) parcel.get('errors');
                                c.API_Response__c += 'ParcelDetails: ' + (String) errors.get('details');
                            }
                        }
                    }
                }
            }
            c.Fetch_Delivery_Status__c = false;
            c.Fetch_parcel_details__c = false;
        }
        update(listCaseQuery);
        if(listCaseContactsToInsert.size()>0){
            insert(listCaseContactsToInsert);
        }
    }
    
    public static CaseAddress__c createCaseContact(Id parentCase, Map<String,Object> details, String contactType, boolean mainContact){
        CaseAddress__c cCon = new CaseAddress__c();
        cCon.Parent_Case__c = parentCase;
        
        String fullName = (String) details.get('name');
        if(fullName!=null){ 
            List<String> names = fullName.split(' ');
            cCon.First_Name__c = names.get(0);
            cCon.Last_Name__c = names.get(names.size()-1);
        }
        cCon.Type__c = contactType;
        cCon.Email_address__c = (String) details.get('email');
        cCon.Phone_number__c = (String) details.get('phone');
        
        Map<String,Object> address = (Map<String,Object>) details.get('address');
        cCon.Street__c = (String) address.get('street');
        cCon.Suburb__c = (String) address.get('suburb');
        cCon.City__c = (String) address.get('city');
        cCon.Postcode__c = (String) address.get('postcode');
        cCon.Country__c = (String) address.get('country');
        cCon.DPID__c = (String) address.get('dpid');
        cCon.Bypassed_by_API__c = true;
        cCon.MainContact__c = mainContact;
        return cCon;
    }
    
    
    //Constructs parameters for making a post callout, uses the response to return the OAuth Token
    public static String getToken() {
        NZ_Post_API_Credentials__c apiSettings = NZ_Post_API_Credentials__c.getInstance('NZ Post Oauth API');
        String endpoint 	= apiSettings.API_URL__c +'?';
        String client_id	= 'client_id='+ apiSettings.client_id__c +'&';
        String grant_type 	= 'grant_type='+'client_credentials'+'&';
        String client_secret= 'client_secret='+ apiSettings.client_secret__c;
        endpoint += client_id ;
        endpoint += grant_type;
        endpoint += client_secret;
        
        HttpResponse response = makePostCallout(endpoint);
        system.debug(response);
        Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
        
        String token = (String) results.get('access_token');
        system.debug('Access_Token:: '+ token); 
        return token;
    }
    
    //Called by getParcelTrackingToken, Returns the HttpResponse containing the OAuth Token
    public static HttpResponse makePostCallout(String endpoint) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setTimeout(30000);
        // Set the body as a JSON object
        HttpResponse response = http.send(request);
        
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
        return response;
    }
    
    //Generic getCallout used for ParcelTracking and ParcelDetails APIs
    public static HttpResponse makeGetCallout(String token, String trackingNo, String endpoint, String clientId) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setTimeout(30000);
        request.setEndpoint(endpoint +trackingNo);
        request.setMethod('GET');
        request.setHeader('Authorization', 'Bearer ' +token);
        request.setHeader('client_id',clientId);
        // Set the body as a JSON object
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
        } else {
            return response;            
        }
        return response;
    }  
}