trigger Case_Trigger on Case (before insert, after insert, after update, before update) {
    
    if ( UserInfo.getUserName() != Label.Migration_User ) {
        Case_Trigger_Handler handler = new Case_Trigger_Handler();
        
        if(Trigger.isUpdate && Trigger.isAfter) {
          handler.OnAfterUpdate(Trigger.New, Trigger.oldMap);      
        }
        
        // SERVICE NZPCRM-409 YK START
        if(Trigger.isBefore && Trigger.isInsert){
            handler.OnBeforeInsert(Trigger.New);
            Case_Trigger_Handler_COM.OnBeforeInsert(Trigger.New);
            
            for(case c : Trigger.new) {
                if (c.NCC_Track_Trace_Number__c!=null) {
                    c.Fetch_Delivery_Status__c=true; 
                    c.Fetch_parcel_details__c=true;
                }
            }
        }
        
        if(Trigger.isBefore && Trigger.isUpdate){
            // SALES YK START
            handler.OnBeforeUpdate(Trigger.New, Trigger.oldMap);
            // SALES YK END
            Case_Trigger_Handler_COM.OnBeforeUpdate(Trigger.New,Trigger.oldMap);
            if (checkRecursion.runOnce()) {
              parcelTrackingStatusHandler.parcelTrackingInit(Trigger.new);
                }
        }
        // SERVICE NZPCRM-409 YK END
        
        // SERVICE NZPCRM-529 YK START
        if(Trigger.isAfter && Trigger.isInsert){
            handler.OnAfterInsert(Trigger.New);
            Case_Trigger_Handler_COM.OnAfterInsert(Trigger.New);
//            parcelTrackingStatusHandler.parcelTrackingInit(Trigger.new);
        }
        // SERVICE NZPCRM-529 YK END
        
        
        if(Trigger.isAfter && Trigger.isUpdate){
          
            Case_Trigger_Handler_COM.OnAfterUpdate(Trigger.New,Trigger.oldMap);

        }
    }
}